import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  needsLoginAsync = true;
  constructor(private auth: AuthenticateService) {}
  ngOnInit() {
    this.auth.isAuthenticatedAsync().then(authenticated => {
      this.needsLoginAsync = !authenticated;
    });
  }

  needsLogin() {
    return !this.auth.isAuthenticated();
  }
}

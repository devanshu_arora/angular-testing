import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
  inject
} from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthenticateService } from '../services/authenticate.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

class MockAuthService extends AuthenticateService {
  isAuthenticated() {
    return true;
  }
}
// describe("LoginComponent", () => {
//   let component: LoginComponent;
//   let fixture: ComponentFixture<LoginComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [LoginComponent]
//     }).compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(LoginComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it("should create", () => {
//     expect(component).toBeTruthy();
//   });
// });
// describe("Component: Login", () => {
//   let component: LoginComponent;
//   let service: AuthenticateService;

//   beforeEach(() => {
//     service = new AuthenticateService();
//     component = new LoginComponent(service);
//   });

//   afterEach(() => {
//     localStorage.removeItem("token");
//     service = null;
//     component = null;
//   });

//   it("canLogin returns false when the user is not authenticated", () => {
//     expect(component.needsLogin()).toBeTruthy();
//   });

//   it("canLogin returns false when the user is not authenticated", () => {
//     localStorage.setItem("token", "12345");
//     expect(component.needsLogin()).toBeFalsy();
//   });
// });

describe('Component: Login', () => {
  let component: LoginComponent;
  let service: AuthenticateService;
  let spy: any;
  let fixture: ComponentFixture<LoginComponent>;
  let elSync: DebugElement;
  let elAsync: DebugElement;

  // beforeEach(() => {
  //   service = new AuthenticateService();
  //   component = new LoginComponent(service);
  // });

  beforeEach(() => {
    // refine the test module by declaring the test component
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [AuthenticateService]
    });

    // create component and test fixture
    fixture = TestBed.createComponent(LoginComponent);

    // get test component from the fixture
    component = fixture.componentInstance;

    // UserService provided to the TestBed
    service = TestBed.get(AuthenticateService);

    elSync = fixture.debugElement.query(By.css('.sync'));
    elAsync = fixture.debugElement.query(By.css('.async'));
  });
  // afterEach(() => {
  //   service = null;
  //   component = null;
  // });

  it('canLogin returns false when the user is not authenticated', () => {
    spy = spyOn(service, 'isAuthenticated').and.returnValue(false);
    expect(component.needsLogin()).toBeTruthy();
    expect(service.isAuthenticated).toHaveBeenCalled();
  });

  it('canLogin returns false when the user is not authenticated', () => {
    spy = spyOn(service, 'isAuthenticated').and.returnValue(true);
    expect(component.needsLogin()).toBeFalsy();
    expect(service.isAuthenticated).toHaveBeenCalled();
  });

  it('login button hidden when the user is authenticated', () => {
    // To being with Angular has not done any change detection so the content is blank.
    expect(elSync.nativeElement.textContent.trim()).toBe('');

    // Trigger change detection and this lets the template update to the initial value which is Login since by
    // default we are not authenticated
    fixture.detectChanges();
    expect(elSync.nativeElement.textContent.trim()).toBe('Login');

    // Change the authetication state to true
    spyOn(service, 'isAuthenticated').and.returnValue(true);

    // The label is still Login! We need changeDetection to run and for angular to update the template.
    expect(elSync.nativeElement.textContent.trim()).toBe('Login');
    // Which we can trigger via fixture.detectChange()
    fixture.detectChanges();

    // Now the label is Logout
    expect(elSync.nativeElement.textContent.trim()).toBe('Logout');
  });

  // without async handling
  it('button async test without async handling', () => {
    fixture.detectChanges();
    expect(elAsync.nativeElement.textContent.trim()).toBe('Login');
    spyOn(service, 'isAuthenticatedAsync').and.returnValue(
      Promise.resolve(true)
    );
    component.ngOnInit();
    fixture.detectChanges();
    expect(elAsync.nativeElement.textContent.trim()).toBe('Logout');
  });

  // async handling with jasmine.done()
  it('button async test with jasmine.done()', done => {
    fixture.detectChanges();
    expect(elAsync.nativeElement.textContent.trim()).toBe('Login');
    const spyI = spyOn(service, 'isAuthenticatedAsync').and.returnValue(
      Promise.resolve(true)
    );
    component.ngOnInit();
    spyI.calls.mostRecent().returnValue.then(() => {
      fixture.detectChanges();
      expect(elAsync.nativeElement.textContent.trim()).toBe('Logout');
      done();
    });
  });

  it('Button label via async() and whenStable()', async(() => {
    fixture.detectChanges();
    expect(elAsync.nativeElement.textContent.trim()).toBe('Login');
    spyOn(service, 'isAuthenticatedAsync').and.returnValue(
      Promise.resolve(true)
    );
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(elAsync.nativeElement.textContent.trim()).toBe('Logout');
    });
    component.ngOnInit();
  }));
  it('Button label via fakeAsync() and tick()', fakeAsync(() => {
    expect(elAsync.nativeElement.textContent.trim()).toBe('');
    fixture.detectChanges();
    expect(elAsync.nativeElement.textContent.trim()).toBe('Login');
    spyOn(service, 'isAuthenticatedAsync').and.returnValue(
      Promise.resolve(true)
    );
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(elAsync.nativeElement.textContent.trim()).toBe('Logout');
  }));
});

describe('Component: Login', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let testBedService: AuthenticateService;
  let componentService: AuthenticateService;

  beforeEach(() => {
    // refine the test module by declaring the test component
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [AuthenticateService]
    });

    // Configure the component with another set of Providers
    TestBed.overrideComponent(LoginComponent, {
      set: {
        providers: [{ provide: AuthenticateService, useClass: MockAuthService }]
      }
    });

    // create component and test fixture
    fixture = TestBed.createComponent(LoginComponent);

    // get test component from the fixture
    component = fixture.componentInstance;

    // AuthService provided to the TestBed
    testBedService = TestBed.get(AuthenticateService);

    // AuthService provided by Component, (should return MockAuthService)
    componentService = fixture.debugElement.injector.get(AuthenticateService);
  });

  it('Service injected via inject(...) and TestBed.get(...) should be the same instance', inject(
    [AuthenticateService],
    (injectService: AuthenticateService) => {
      expect(injectService).toBe(testBedService);
    }
  ));

  it('Service injected via component should be and instance of MockAuthService', () => {
    expect(componentService instanceof MockAuthService).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTestingComponent } from './form-testing.component';
import { User } from '../login2/login2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('FormTestingComponent', () => {
  let component: FormTestingComponent;
  let fixture: ComponentFixture<FormTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [FormTestingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('email field validity', () => {
    const email = component.form.controls['email'];
    expect(email.valid).toBeFalsy();
    let errors = {};
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy();
  });
  it('email field validity', () => {
    const email = component.form.controls['email'];
    // expect(email.valid).toBeFalsy();
    let errors = {};
    // errors = email.errors || {};
    // expect(errors['required']).toBeTruthy();
    email.setValue('test');
    errors = email.errors || {};
    expect(errors['pattern']).toBeTruthy();
  });

  it('submitting a form emits a user', () => {
    expect(component.form.valid).toBeFalsy();
    component.form.controls['email'].setValue('test@test.com');
    component.form.controls['password'].setValue('123456789');
    expect(component.form.valid).toBeTruthy();

    let user: User;
    // Subscribe to the Observable and store the user in a local variable.
    component.loggedIn.subscribe(value => (user = value));

    // Trigger the login function
    component.login();

    // Now we can check to make sure the emitted value is correct
    expect(user.email).toBe('test@test.com');
    expect(user.password).toBe('123456789');
  });
});

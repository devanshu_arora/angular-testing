import { TestBed, inject } from '@angular/core/testing';

import { AuthenticateService } from './authenticate.service';

describe('AuthenticateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticateService]
    });
  });

  it('should be created', inject(
    [AuthenticateService],
    (service: AuthenticateService) => {
      expect(service).toBeTruthy();
    }
  ));
});
describe('Service: Auth', () => {
  let service: AuthenticateService;

  beforeEach(() => {
    service = new AuthenticateService();
  });

  afterEach(() => {
    service = null;
    localStorage.removeItem('token');
  });

  it('should return true from isAuthenticated when there is a token', () => {
    localStorage.setItem('token', '1234');
    expect(service.isAuthenticated()).toBeTruthy();
  });

  it('should return false from isAuthenticated when there is no token', () => {
    expect(service.isAuthenticated()).toBeFalsy();
  });
});

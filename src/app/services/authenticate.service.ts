import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticateService {
  isAuthenticated(): boolean {
    return !!localStorage.getItem('token');
  }
  isAuthenticatedAsync(): Promise<boolean> {
    return Promise.resolve(!!localStorage.getItem('token'));
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { Login2Component } from './login2/login2.component';
import { HoverFocusDirective } from './utilities/directive/hover-focus.directive';
import { TestDirectiveComponent } from './test-directive/test-directive.component';
import { FormTestingComponent } from './form-testing/form-testing.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { AppRoutingModule } from './app-routing.module';
import { RouteContainerComponent } from './route-container/route-container.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Login2Component,
    HoverFocusDirective,
    TestDirectiveComponent,
    FormTestingComponent,
    HomeComponent,
    SearchComponent,
    RouteContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
